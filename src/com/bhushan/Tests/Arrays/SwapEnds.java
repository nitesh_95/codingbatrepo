package com.bhushan.Tests.Arrays;

import java.util.Arrays;

/**
 * Given an array of ints, swap the first and last elements in the array. Return
 * the modified array. The array length will be at least 1. swapEnds([1, 2, 3,
 * 4]) → [4, 2, 3, 1] swapEnds([1, 2, 3]) → [3, 2, 1] swapEnds([8, 6, 7, 9, 5])
 * → [5, 6, 7, 9, 8]
 *
 */
public class SwapEnds {
	public static int[] swapEnds(int[] nums) {
		int i, temp;
		for (i = 0; i < nums.length / 2; i++) {
			temp = nums[i];
			nums[i] = nums[nums.length - i - 1];
			nums[nums.length - i - 1] = temp;
		}
		System.out.println(Arrays.toString(nums));
		return nums;
	}

	public static void main(String[] args) {
		int array1[] = { 1, 2, 3, 4 };
		swapEnds(array1);
	}
}
