package com.bhushan.Tests.Arrays;

import java.util.Arrays;

/**
 * Given an array of ints of odd length, look at the first, last, and middle
 * values in the array and return the largest. The array length will be a least
 * 1. maxTriple([1, 2, 3]) → 3 maxTriple([1, 5, 3]) → 5 maxTriple([5, 2, 3]) → 5
 */
public class MaxTriple {
	public static int maxTriple(int[] nums) {
		Arrays.sort(nums);
		System.out.println("sorted Array ::" + Arrays.toString(nums));
		int res = nums[nums.length - 1];
		System.out.println("largest element is :" + res);
		return res;

	}

	public static void main(String[] args) {
		int array1[] = { 1, 5, 3 };
		maxTriple(array1);
	}

}
