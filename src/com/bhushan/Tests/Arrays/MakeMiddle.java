package com.bhushan.Tests.Arrays;

import java.util.Arrays;

/**
 * Given an array of ints of even length, return a new array length 2 containing
 * the middle two elements from the original array. The original array will be
 * length 2 or more. makeMiddle([1, 2, 3, 4]) → [2, 3] makeMiddle([7, 1, 2, 3,
 * 4, 9]) → [2, 3] makeMiddle([1, 2]) → [1, 2]
 *
 */
public class MakeMiddle {
	public static int[] makeMiddle(int[] nums) {
		int numsdata;
		numsdata = ((nums.length) / 2 - 1);
		int arr[] = { nums[numsdata], nums[numsdata + 1] };
		System.out.println(Arrays.toString(arr));
		return arr;
	}

	public static void main(String[] args) {
		int array1[] = {  1, 2};
		makeMiddle(array1);
	}

}
