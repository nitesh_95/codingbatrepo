package com.bhushan.Tests.Arrays;

import java.util.Arrays;

import org.apache.commons.lang3.ArrayUtils;

/**
 * @author Given an int array length 3, if there is a 2 in the array immediately
 *         followed by a 3, set the 3 element to 0. Return the changed array.
 *         fix23([1, 2, 3]) → [1, 2, 0] fix23([2, 3, 5]) → [2, 0, 5] fix23([1,
 *         2, 1]) → [1, 2, 1]
 *
 */
public class Fix23 {
	public static int[] fix23(int[] nums) {
		int j =0;
		int indexOfTwo = ArrayUtils.indexOf(nums, 2);
		int indexofThree = indexOfTwo + 1;
		
		for (int i = 0; i < nums.length; i++) {
			if (nums[indexOfTwo] == 2 && nums[indexofThree] == 3) {
				 nums[indexofThree] = j;
			} else {
				 nums[indexofThree] = j;
			}
			
		}
		System.out.println(Arrays.toString(nums));
		return nums;

		  
	}
	public static void main(String[] args) {
		int[] num = { 2, 3, 5};
		fix23(num);
	}
}
