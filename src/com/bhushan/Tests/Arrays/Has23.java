package com.bhushan.Tests.Arrays;

/**
 * Given an int array length 2, return true if it contains a 2 or a 3. has23([2,
 * 5]) → true has23([4, 3]) → true has23([4, 5]) → false
 *
 */
public class Has23 {

	public static boolean has23(int[] nums) {
		String numsdata = null;
		for (int i = 0; i < nums.length; i++) {
			if (nums[i] == 2 || nums[i] == 3) {
				numsdata = "true";
			} else {
				numsdata = "false";
			}
		}
		if (numsdata == "true") {
			System.out.println(numsdata);
		} else {
			System.out.println("false");
		}
		return false;
	}

	public static void main(String[] args) {
		int[] num = { 2, 3 };
		has23(num);
	}
}
