package com.bhushan.Tests.Arrays;

/**
 * Given an int array, return true if the array contains 2 twice, or 3 twice.
 * The array will be length 0, 1, or 2. double23([2, 2]) → true double23([3, 3])
 * → true double23([2, 3]) → false
 *
 */
public class Double23 {

	public static boolean double23(int[] nums) {
		String value = null;
		for (int i = 0; i < nums.length; i++) {
			if (nums[i] == 2 || nums[i] == 3 && (nums[i] + 1) == 2 || (nums[i] + 1) == 3) {
				value = "true";
			} else {
				value = "false";
			}
		}
		System.out.println(value);
		return false;
	}

	public static void main(String[] args) {
		int array1[] = { 2, 3 };
		double23(array1);
	}
}
