package com.bhushan.Tests.Arrays;

import java.util.Arrays;

/**
 * @Given 2 int arrays, a and b, each length 3, return a new array length 2
 *        containing their middle elements. middleWay([1, 2, 3], [4, 5, 6]) →
 *        [2, 5] middleWay([7, 7, 7], [3, 8, 0]) → [7, 8] middleWay([5, 2, 9],
 *        [1, 4, 5]) → [2, 4]
 *
 */
public class MiddleWay {

	public static int[] middleWay(int[] a, int[] b) {
		int firstelement = 0, secondelement = 0;
		for (int i = 0; i < a.length; i++) {
			int midelement = ((a.length + 1) / 2) - 1;
			firstelement = a[midelement];
		}
		for (int i = 0; i < b.length; i++) {
			int midelement1 = ((b.length + 1) / 2) - 1;
			secondelement= b[midelement1];
		}

		int array[] = { firstelement, secondelement };
		System.out.println(Arrays.toString(array));
		return array;
	}

	public static void main(String[] args) {
		int array1[] = { 1, 2, 3 };
		int array2[] = { 1, 4, 3 };
		middleWay(array1, array2);
	}

}
