package com.bhushan.Tests.Arrays;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Given an array of ints length 3, return a new array with the elements in
 * reverse order, so {1, 2, 3} becomes {3, 2, 1}. reverse3([1, 2, 3]) → [3, 2,
 * 1] reverse3([5, 11, 9]) → [9, 11, 5] reverse3([7, 0, 0]) → [0, 0, 7]
 *
 */
public class ReverseElementArray {
	public static Integer[] reverseArray(Integer[] nums) {
		List<Integer> list = Arrays.asList(nums);
		Collections.reverse(list);
		System.out.println(list);
		return nums;
	}

	public static void main(String[] args) {
		Integer[] num = { 1, 2, 3 };
		reverseArray(num);
	}
}
