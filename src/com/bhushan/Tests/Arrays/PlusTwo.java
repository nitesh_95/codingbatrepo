package com.bhushan.Tests.Arrays;

import java.util.Arrays;

/**
 * Given 2 int arrays, each length 2, return a new array length 4 containing all
 * their elements. plusTwo([1, 2], [3, 4]) → [1, 2, 3, 4] plusTwo([4, 4], [2,
 * 2]) → [4, 4, 2, 2] plusTwo([9, 2], [3, 4]) → [9, 2, 3, 4]
 *
 */
public class PlusTwo {

	public static int[] plustwo(int[] nums, int[] numsdata) {
		int[] myArray = { nums[0], nums[1], numsdata[0], numsdata[1] };
		System.out.println(Arrays.toString(myArray));
		return myArray;

	}

	public static void main(String[] args) {
		int[] nums = { 1, 2 };
		int[] numsdata = { 3, 4 };
		plustwo(nums, numsdata);
	}
}
