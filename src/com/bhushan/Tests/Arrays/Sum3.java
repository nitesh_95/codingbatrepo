package com.bhushan.Tests.Arrays;

/**
 * Given an array of ints length 3, return the sum of all the elements. sum3([1,
 * 2, 3]) → 6 sum3([5, 11, 2]) → 18 sum3([7, 0, 0]) → 7
 *
 */
public class Sum3 {

	public static int sum3(int[] nums) {
		int sum = 0;
		for (int i = 0; i < nums.length; i++) {
			sum = nums[i] + sum;
		}
		System.out.println(sum);
		return sum;
	}

	public static void main(String[] args) {
		int sum[] = { 1, 2, 3 };
		sum3(sum);
	}
}
