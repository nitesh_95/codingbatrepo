package com.bhushan.Tests.Arrays;

/**
 * Start with 2 int arrays, a and b, of any length. Return how many of the
 * arrays have 1 as their first element. start1([1, 2, 3], [1, 3]) → 2
 * start1([7, 2, 3], [1]) → 1 start1([1, 2], []) → 1
 *
 */
public class Start1 {

	public static int start1(int[] a, int[] b) {

		int num;
		if (a[0] == 1 && b[0] == 1) {
			num = 2;
			System.out.println(num);
			return num;
		} else if (a[0] == 1 || b[0] == 1) {
			num = 1;
			System.out.println(num);
			return num;
		} else {
			num = 0;
			System.out.println(num);
			return num;
		}
	}

	public static void main(String[] args) {
		int array1[] = { 1, 2, 3 };
		int array2[] = { 1, 3 };
		start1(array1, array2);
	}
}