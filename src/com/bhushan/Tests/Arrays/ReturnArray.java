package com.bhushan.Tests.Arrays;


import java.util.Arrays;
import java.util.Scanner;

/**
 * @author
 * Return an int array length 3 containing the first 3 digits of pi, {3, 1, 4}.
makePi() → [3, 1, 4]
 *
 */
public class ReturnArray {

	@SuppressWarnings("resource")
	public static int[] makePi() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter size");
		int size = scanner.nextInt();

		int[] arr = new int[size];
		for (int j = 0; j < arr.length; j++) {
			System.out.println("Enter Elements" + j);
			arr[j] = scanner.nextInt();
		}
		System.out.println(Arrays.toString(arr));
		return arr;
	}

	public static void main(String[] args) {
		makePi();
	}
}
