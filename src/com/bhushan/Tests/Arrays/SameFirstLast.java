package com.bhushan.Tests.Arrays;

/**
 * @Given an array of ints, return true if the array is length 1 or more, and
 *        the first element and the last element are equal. sameFirstLast([1, 2,
 *        3]) → false sameFirstLast([1, 2, 3, 1]) → true sameFirstLast([1, 2,
 *        1]) → true
 *
 */
public class SameFirstLast {

	public static boolean sameFirstLast(int[] nums) {
		System.out.println(nums[nums.length - 1]);
		if (nums[0] == nums[nums.length - 1] && nums.length > 1) {
			System.out.println(true);
			return true;
		} else {
			System.out.println(false);
			return false;
		}

	}

	public static void main(String[] args) {
		int[] nums = { 1, 2, 3 };
		int[] numbers = { 1, 2, 3, 1 };
		int[] numsData = { 1, 2, 1 };
		sameFirstLast(nums);
		sameFirstLast(numbers);
		sameFirstLast(numsData);
	}

}
