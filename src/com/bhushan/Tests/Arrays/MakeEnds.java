package com.bhushan.Tests.Arrays;

import java.util.Arrays;

/**
 * Given an array of ints, return a new array length 2 containing the first and
 * last elements from the original array. The original array will be length 1 or
 * more. makeEnds([1, 2, 3]) → [1, 3] makeEnds([1, 2, 3, 4]) → [1, 4]
 * makeEnds([7, 4, 6, 2]) → [7, 2]
 *
 */
public class MakeEnds {

	public static int[] makeEnds(int[] nums) {

		int i = nums[0];
		int j = nums[nums.length - 1];

		int array[] = { i, j };
		System.out.println(Arrays.toString(array));
		return array;

	}

	public static void main(String[] args) {
		int array1[] = {7, 4, 6, 2};
		makeEnds(array1);
	}
}
