package com.bhushan.Tests.Arrays;

import java.util.Arrays;

/**
 * Given an int array of any length, return a new array of its first 2 elements.
 * If the array is smaller than length 2, use whatever elements are present.
 * frontPiece([1, 2, 3]) → [1, 2] frontPiece([1, 2]) → [1, 2] frontPiece([1]) →
 * [1]
 *
 */
public class FrontPiece {
	public static int[] frontPiece(int[] nums) {
		if (nums.length > 1) {
			int array[] = { nums[0], nums[1] };
			System.out.println(Arrays.toString(array));
		} else {
			int array[] = { nums[0] };
			System.out.println(Arrays.toString(array));
		}
		return nums;
	}

	public static void main(String[] args) {
		int[] num = { 1, 2, 3 };
		frontPiece(num);
	}
}
