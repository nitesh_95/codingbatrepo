package com.bhushan.Tests.Arrays;

import java.text.DecimalFormat;

/**
 * @author Trim decimal to certain place
 *
 */
public class TrimDecimal {

	public static void main(String[] args) {
		double d = 17.5345;
		DecimalFormat df = new DecimalFormat("#.##");
		System.out.print(df.format(d));
	}
}
