package com.bhushan.Tests.Arrays;

import org.apache.commons.lang3.ArrayUtils;

/**
 * We'll say that a 1 immediately followed by a 3 in an array is an "unlucky" 1.
 * Return true if the given array contains an unlucky 1 in the first 2 or last 2
 * positions in the array. unlucky1([1, 3, 4, 5]) → true unlucky1([2, 1, 3, 4,
 * 5]) → true unlucky1([1, 1, 1]) → false
 *
 */
public class Unlucky1 {
	public static boolean unlucky1(int[] nums) {
		String value = null;
		int indexOfTwo = ArrayUtils.indexOf(nums, 1);
		int indexofThree = indexOfTwo + 1;
		
		for (int i = 0; i < nums.length; i++) {
			if (nums[indexOfTwo] == 1 && nums[indexofThree] == 3) {
				value = "true";
			} else {
				value = "false";
			}
		}
		System.out.println(value);
		return false;

	}

	public static void main(String[] args) {
		int array1[] = { 1, 1, 1};
		unlucky1(array1);
	}
}
