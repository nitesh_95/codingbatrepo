package com.bhushan.Tests.Arrays;

import java.util.Arrays;

/**
 * Given 2 int arrays, a and b, of any length, return a new array with the first
 * element of each array. If either array is length 0, ignore that array.
 * front11([1, 2, 3], [7, 9, 8]) → [1, 7] front11([1], [2]) → [1, 2] front11([1,
 * 7], []) → [1]
 *
 */
public class Front11 {
	public static int[] front11(int[] a, int[] b) {
		int arrays[] = {};
		if (a.length > 0 && b.length > 0) {
			int[] arr = { a[0], b[0] };
			System.out.println(Arrays.toString(arr));
		} else if (a.length > 1 && b.length == 0) {
			System.out.println(Arrays.toString(a));
		} else if (a.length == 0 && b.length > 1) {
			System.out.println(Arrays.toString(b));
		}
		return arrays;
	}

	public static void main(String[] args) {
		int[] a = { 1, 7 };
		int[] b = {0};
		front11(a, b);
	}
}
