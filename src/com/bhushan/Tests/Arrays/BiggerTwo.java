package com.bhushan.Tests.Arrays;

import java.util.Arrays;

/**
 * Start with 2 int arrays, a and b, each length 2. Consider the sum of the
 * values in each array. Return the array which has the largest sum. In event of
 * a tie, return a. biggerTwo([1, 2], [3, 4]) → [3, 4] biggerTwo([3, 4], [1, 2])
 * → [3, 4] biggerTwo([1, 1], [1, 2]) → [1, 2]
 *
 */
public class BiggerTwo {

	public static int[] biggerTwo(int[] a, int[] b) {
		int sum1 = 0, sum2 = 0;
		for (int i = 0; i < a.length; i++) {
			sum1 = sum1 + a[i];
		}
		for (int i = 0; i < b.length; i++) {
			sum2 = sum2 + b[i];
		}
		if (sum1 > sum2) {
			System.out.println(Arrays.toString(a));
			return a;
		} else {
			System.out.println(Arrays.toString(b));
			return b;
		}

	}

	public static void main(String[] args) {
		int array1[] = { 1, 2, 3 };
		int array2[] = { 1, 4, 3 };

		biggerTwo(array1, array2);
	}

}
